#-------------------------------------------------
#
# Project created by QtCreator 2015-03-02T16:35:14
#
#-------------------------------------------------

QT       += core network

QT       -= gui

CONFIG += c++11

TARGET = ssltest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp
