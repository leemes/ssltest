#include <QCoreApplication>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QSslConfiguration>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QNetworkAccessManager nam;

    QSslConfiguration config = QSslConfiguration::defaultConfiguration();

    // Here you can change the SSL configuration used for the request...

    QNetworkReply *reply = nam.get(QNetworkRequest(QUrl("https://google.com/")));
    reply->setSslConfiguration(config);

    QObject::connect(&nam, &QNetworkAccessManager::encrypted, [](QNetworkReply *reply) {
        qDebug() << reply->sslConfiguration().peerCertificate();
    });

    QObject::connect(&nam, &QNetworkAccessManager::sslErrors, [](QNetworkReply *reply, QList<QSslError> errors) {
        for (auto e : errors)
            qDebug() << "[SSL error]" << e.errorString();
        qDebug() << reply->sslConfiguration().peerCertificate();
    });

    return a.exec();
}
